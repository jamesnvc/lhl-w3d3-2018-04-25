//
//  ViewController.m
//  CollectionViewsDemo
//
//  Created by James Cash on 25-04-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "RedCollectionViewCell.h"

@interface ViewController () <UICollectionViewDataSource>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong,nonatomic) UICollectionViewFlowLayout *smallLayout;
@property (strong,nonatomic) UICollectionViewFlowLayout *bigLayout;
@property (strong,nonatomic) UICollectionViewLayout *storyboardLayout;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.smallLayout = [[UICollectionViewFlowLayout alloc] init];
    self.smallLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.smallLayout.itemSize = CGSizeMake(50, 50);
    self.smallLayout.minimumInteritemSpacing = 5;
    self.smallLayout.minimumLineSpacing = 0;
    self.smallLayout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5);
    self.smallLayout.headerReferenceSize = CGSizeMake(0, 22);
    self.smallLayout.footerReferenceSize = CGSizeMake(0, 15);

    self.bigLayout = [[UICollectionViewFlowLayout alloc] init];
    self.bigLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.bigLayout.itemSize = CGSizeMake(250, 150);
    self.bigLayout.minimumInteritemSpacing = 20;
    self.bigLayout.minimumLineSpacing = 15;
    self.bigLayout.sectionInset = UIEdgeInsetsMake(10, 20, 10, 20);
    self.bigLayout.headerReferenceSize = CGSizeMake(80, 0);
    self.bigLayout.footerReferenceSize = CGSizeMake(50, 0);

    self.storyboardLayout = self.collectionView.collectionViewLayout;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Toggling layout

- (IBAction)toggleLayout:(id)sender {
    UICollectionViewLayout *next,
    *old = self.collectionView.collectionViewLayout;

    if (old == self.storyboardLayout) {
        next = self.smallLayout;
    } else if (old == self.smallLayout) {
        next = self.bigLayout;
    } else {
        next = self.storyboardLayout;
    }

    [self.collectionView setCollectionViewLayout:next animated:YES];

}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 3;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return section + 5;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RedCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"OurCell" forIndexPath:indexPath];

    cell.theLabel.text = [NSString stringWithFormat:@"S %ld I %ld", indexPath.section, indexPath.item];

    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView* header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"OurHeader"                                                                                     forIndexPath:indexPath];
        UILabel *lbl = [header viewWithTag:1];
        lbl.text = [NSString stringWithFormat:@"Welcome to section %ld!", indexPath.section];
        return header;
    }
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        UICollectionReusableView* footer = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"OurFooter" forIndexPath:indexPath];
        UILabel *lbl = [footer viewWithTag:1];
        lbl.text = [NSString stringWithFormat:@"Now leaving section %ld", indexPath.section];
        return footer;
    }
    return nil;
}

@end
