//
//  RedCollectionViewCell.h
//  CollectionViewsDemo
//
//  Created by James Cash on 25-04-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RedCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *theLabel;

@end
